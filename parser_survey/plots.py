import seaborn as sns
import json
import pandas as pd
from collections import defaultdict

ARGUMENT_PARSERS = ['argparse', 'optparse', 'argv', 'click', 'docopt']
COLUMNS = ['argument_parser', 'occurences']
with open('joined.json') as f:
    data = json.load(f)
results = defaultdict(int)
for repo in data:
    for key in ARGUMENT_PARSERS:
        if key in repo.keys():
            results[key] += repo[key]
results = dict(results)
print(results)
df1 = pd.DataFrame(list(results.items()), columns=COLUMNS)
print(df1)
sns.barplot(x='argument_parser', y='occurences', data=df1)

df2 = pd.DataFrame(columns=COLUMNS.append('downloads'))
for i, repo in enumerate(data):
    for key in ARGUMENT_PARSERS:
        if key in repo.keys():
            df2.loc[i] = [key, repo[key], repo['ndownloads']]
print(df2)
sns.swarmplot(x='argument_parser', y='downloads', data=df2)