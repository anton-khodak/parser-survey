#!/usr/bin/env python
# -*- coding: utf-8 -*-
import scrapy
from urlparse import urlparse

from parser_survey.items import PyPiRepoItem


class RepositorySpider(scrapy.Spider):
    name = "repository"
    allowed_domains = ['pypi.python.org']
    start_urls = [
        "https://pypi.python.org/pypi?:action=browse&show=all&c=385&c=388",
    ]

    def parse(self, response):
        for href in response.xpath("/html/body/div[5]/div/div[1]/div[3]/table[2]/tbody/tr/td/a/@href"):
            url = response.urljoin(href.extract())
            yield scrapy.Request(url, callback=self.parse_github_links)

    def parse_github_links(self, response):
        github_link = response.xpath(".//*[@id='content']/div[3]/ul[2]/li[2]/a/@href")
        try:
            url = github_link.extract()[0]
            parsed_uri = urlparse(url)
            domain = '{uri.scheme}://{uri.netloc}/'.format(uri=parsed_uri)
            # not github repository
            if not 'github.com' in domain:
                return
        # No link to repository at all
        except IndexError:
            return
        item = PyPiRepoItem()
        item['name'] = response.xpath(".//*[@id='content']/div[3]/h1/text()").extract()[0].split()[0]
        item['description'] = response.xpath(".//*[@id='content']/div[3]/p[1]/text()").extract()[0]
        item['link'] = url
        item['repo'] = '/'.join(parsed_uri.path.strip('/').split('/')[:2])  # leaves only 'username/repository'
        item['ndownloads'] = int(response.xpath(".//*[@id='content']/div[3]/ul[1]/li[4]/span/text()").extract()[0])
        return item
