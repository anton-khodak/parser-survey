# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

import scrapy


class RepositoryItem(scrapy.Item):
    argparse = scrapy.Field()
    optparse = scrapy.Field()
    argv = scrapy.Field()
    click = scrapy.Field()
    docopt = scrapy.Field()


class PyPiRepoItem(scrapy.Item):
    name = scrapy.Field()
    description = scrapy.Field()
    link = scrapy.Field()
    repo = scrapy.Field()
    ndownloads = scrapy.Field()
