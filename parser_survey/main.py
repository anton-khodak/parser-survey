import json
import time
from collections import defaultdict
from pprint import pprint

from requests_oauthlib.oauth2_session import OAuth2Session

ARGUMENT_PARSERS = ['argparse', 'optparse', 'argv', 'click', 'docopt']


data_file = 'left.json'
client_id = '28ce6746118d99cf0f65'
client_secret = 'bbe475bca29b598750fafccd560d841c9b84f51c'
authorization_base_url = 'https://github.com/login/oauth/authorize'
token_url = 'https://github.com/login/oauth/access_token'
github = OAuth2Session(client_id)

# Redirect user to GitHub for authorization
# AWFUL, but I couldn't find a proper way in a reasonable time
authorization_url, state = github.authorization_url(authorization_base_url)
print 'Please go here and authorize,', authorization_url

# Get the authorization verifier code from the callback url
redirect_response = raw_input('Paste the full redirect URL here:')

# Fetch the access token
github.fetch_token(token_url, client_secret=client_secret,
                   authorization_response=redirect_response)

# Checking authentication
r = github.get('https://api.github.com/user')
print r.content

with open(data_file) as f:
    data = json.load(f)

results = defaultdict(int)
counter = 0
for repo in data:
    for argparser in ARGUMENT_PARSERS:
        if argparser == 'argv':
            # checking `import sys` is not appropriate
            search_string = argparser
        else:
            search_string = 'import+' + argparser
        url = 'https://api.github.com/search/code?q=' + search_string + '+in:file+language:python+repo:' + repo['repo']
        r = github.get(url)
        occurences = 0
        try:
            occurences = r.json()['total_count']
            results[argparser] += occurences
        except KeyError:  # happens when a response returns an error
            pass
        message = r.json().get('message', 'OK') # if 'message' in response - something went wrong
        pprint(message)
        if message != 'OK':
            pprint(r.json().get('errors', ''))
        pprint(repo['link'])
        pprint(argparser)
        if occurences:
            print 'FOUND!1111111111111111111111111111111111111111111111111'
            try:
                repo[argparser] += occurences
            except KeyError:
                repo[argparser] = occurences
        pprint(results)
        counter += 1
        if counter % 29 == 0:
            # no more than 30 requests in a minute - GitHub limitation
            # should go to sleep for that time
            time.sleep(60)
print 'Processed %i items' % len(data)
print 'Results: '
pprint(results)
with open('output.json', 'a') as fp:
    json.dump(data, fp)
