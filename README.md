# README #

### What is this repository for? ###
Fetching links to bioinformatic repositories on GitHub from PyPi and gathering statistics on which argument parsers they use. Can be applied to any list of GitHub repositories to check which of them and how often do they use particular argument parsers.


### Data ###

joined.json - full statictics for list of repositories from https://pypi.python.org/pypi?:action=browse&show=all&c=385&c=388 which have GitHub links in their descriptions


### Dependencies ###

* Python 2.7
* Scrapy==1.05
* Requests
* Requests-OAuthLib
* seaborn